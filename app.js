var io = new (require('socket.io'))()
//library loader
var loadLib = function(lib){
    return require(__dirname+'/libs/'+lib+'.js')
}
//process handlers
var s = loadLib('process')(process,__dirname)
//configuration loader
var config = loadLib('config')(s)
//basic functions
loadLib('basic')(s,config)
//socket.io
loadLib('webSocket')(s,config,io)
//express web server with ejs
var app = loadLib('webServer')(s,config,io)
//user management
loadLib('notification')(s,config,io,app)
//raspivid
loadLib('raspi')(s,config,io,app)
