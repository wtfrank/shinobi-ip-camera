module.exports = function(s){
    s.location = {
        config : s.mainDirectory+'/conf.js',
    }
    try{
        var config = require(s.location.config)()
    }catch(err){
        console.log(err)
        var config = {}
    }

    // branding >
    if(config.showPoweredByShinobi === undefined){config.showPoweredByShinobi=true}
    if(config.poweredByShinobi === undefined){config.poweredByShinobi='Powered by Shinobi Systems'}
    if(config.poweredByShinobiClass === undefined){config.poweredByShinobiClass='margin:15px 0 0 0;text-align:center;color:#777;font-family: sans-serif;text-transform: uppercase;letter-spacing: 3;font-size: 8pt;'}
    if(config.webPageTitle === undefined){config.webPageTitle='Shinobi License Management System'}
    if(config.webPageTitle === undefined){config.webPageDescription='Shinobi License Management System'}
    if(config.webFavicon === undefined){config.webFavicon='libs/img/brand/favicon.ico'}
    if(config.logoLink === undefined){config.logoLink='libs/img/brand/logo.png'}
    if(config.homeLogoStyle === undefined){config.homeLogoStyle='width:75px;border-radius:50%'}
    // if(config.logoLocation76x76 === undefined){config.logoLocation76x76='libs/img/icon/apple-touch-icon-76x76.png'}
    // if(config.logoLocation76x76Link === undefined){config.logoLocation76x76Link='https://Shinobi.systems'}
    // if(config.logoLocation76x76Style === undefined){config.logoLocation76x76Style='border-radius:50%'}

    s.getConfigWithBranding = function(domain){
        var configCopy = Object.assign({},config)
        if(config.brandingConfig && config.brandingConfig[domain]){
            return Object.assign(configCopy,config.brandingConfig[domain])
        }
        return config
    }
    // branding />
    if(config.port === undefined){config.port = 80}
    if(!config.licenseEndpoint)config.licenseEndpoint = 'https://licenses.shinobi.video'

    return config
}
