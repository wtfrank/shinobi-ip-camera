# Raspbian OS
sudo apt update -y
sudo apt install ffmpeg -y
wget https://deb.nodesource.com/setup_12.x
chmod +x setup_12.x
./setup_12.x || exit $?
sudo apt install nodejs -y
sudo apt install node-pre-gyp -y
rm setup_12.x*

npm install --unsafe-perm
npm audit fix --force
npm install pm2 -g --unsafe-perm

git clone https://github.com/iizukanao/node-rtsp-rtmp-server.git
cd node-rtsp-rtmp-server
npm install -d
coffee -c *.coffee
pm2 start server.js

cd ..
pm2 start app.js
pm2 startup
pm2 save
